/**
 * @author Afranio Martins<afranioce@gmail.com>
 */
var _ = require('Avanz/lib/underscore');
/**
 * Creates a new CreateBuilder.
 * @class Represents a CreateBuilder.
 * @returns {CreateBuilder}
 */
function CreateBuilder() {
    "use strict";

    this.VERSION = '0.1';

    this.EXISTS = 'IF EXISTS ';
    this.NOT_EXISTS = 'IF NOT EXISTS ';

    var _table = '';
    var _columns = {};
    var _unique;
    var _dropTable = false;
    //    var _index;
    var _primary;
    var _foreing = [];


    var _dataTypes = {
        'string': 'TEXT',
        'float': 'REAL',
        'integer': 'INTEGER', //A column declared INTEGER PRIMARY KEY will autoincrement.
        'decimal': 'NUMERIC'
    };

    var _exists = false;
    var _notExists = false;

    var that = this;

    this.execute = function() {
        var QueryBuilder = require('Avanz/misc/QueryBuilder');
        var builder = new QueryBuilder();

        var result = builder.execute(this.getSQL());
        
        this.resetQueryParts();

        return result;
    };

    this.resetQueryParts = function(){
        _exists = false;
        _notExists = false;
        _table = '';
        _columns = {};
        _unique;
        _dropTable = false;
    //    var _index;
        _primary;
        _foreing = [];
    }

    this.create = function(table) {
        if (_.isUndefined(table) || !_.isString(table)) {
            return this;
        }
    
        _table = table;

        return this;
    };

    this.drop = function(table){
        if (_.isUndefined(table) || !_.isString(table)) {
            return this;
        }

        _dropTable = true;
        _table = table;

        return this;
    };

    this.getSQL = function() {
        if(_dropTable === true)
            return getQueryDrop();
        else
            return getQueryCreate();
    };

    var getQueryDrop = function(){
        var query = 'DROP TABLE '
        + (_exists === true ? that.EXISTS : '')
        + (_notExists === true ? that.NOT_EXISTS : '')
        + _table
        return query;
    };

    var getQueryCreate = function(){
        var query = 'CREATE TABLE '

        + (_exists === true ? that.EXISTS : '')
        + (_notExists === true ? that.NOT_EXISTS : '')
        + _table
        + ' (' + prepareFiels()
        + (!_.isEmpty(_primary) ? ', ' + _primary : '')
        + (!_.isEmpty(_foreing) && _.isArray(_foreing) ? ', ' + _foreing.join(', ') : '')
        + (!_.isEmpty(_unique) ? ', ' + _unique : '')
        + ')';

        return query;
    };

    this.columns = function(columns) {
        if (_.isUndefined(columns) || !_.isObject(columns)) {
            throw new Error('Error: columns is not object');
        }

        _columns = columns;
        return this;
    };

    this.primaryKey = function(column) {
        if (_.isUndefined(column)) {
            return this;
        }

        _primary = 'PRIMARY KEY (' + implode(', ', _.isArray(column) ? column : arguments) + ')';

        return this;
    };

    this.exists = function() {
        _exists = true;
        return this;
    };

    this.notExists = function() {
        _notExists = true;
        return this;
    };

    this.uniqueKey = function(column) {
        if (_.isUndefined(column)) {
            return this;
        }

        _unique = 'UNIQUE (' + implode(', ', _.isArray(column) ? column : arguments) + ')';

        return this;
    };

    /*
     this.indexes = function(column){
     if (_.isUndefined(column)) {
     return this;
     }

     _index = 'INDEX (' + implode(', ', _.isArray(column) ? column : arguments) + ')';
     _index = _.isArray(column) ? column : arguments;

     return this;
     }
     */
    /**
     * @example
     * .foreingKey({
     *   'fk_index_name': {
     *      'table' => 'node_revision',
     *      'columns' => array('vid' => 'vid'),
     *   }
     * })
     * @param {type} fields
     * @returns {CreateBuilder|CreateBuilder}
     * FOREIGN KEY(songartist, songalbum) REFERENCES album(albumartist, albumname)
     */
    this.foreingKey = function(fields) {
        if (_.isUndefined(fields)) {
            return this;
        }

        if (_.isObject(fields) && !_.isArray(fields)) {

            _.each(fields, function(item, key){
                _foreing.push('FOREIGN KEY (' + key + ') REFERENCES ' + item.table + '(' + item.column + ')');
            });

            return this;
        } else {
            throw new Error('Error: columns is not array');
        }
    };

    var prepareFiels = function() {
        var f = [];
        _.each(_columns, function(values, column) {
            var _column = getOptions(column, values);

            f.push(_column);
        });

        return f.join(', ');
    };

    var getOptions = function(column, values) {
        column;
        _.each(values, function(value, key) {
            column += ' ';
            switch (key) {
                case 'type':
                    if (_dataTypes[value] === _dataTypes['decimal'])
                        _dataTypes[value] = _dataTypes[value] + '(' + values['format'] + ')';

                    column += _dataTypes[value];
                    break;
                case 'null':
                    column += (value === false ? 'NOT NULL' : '');
                    break;
                case 'default':
                    column += value;
                    break;
            }
        });
        return column.trim();
    };
}

var implode = function(separator, array) {
    var a = _.toArray(array);
    if (!_.isEmpty(a))
        return a.length === 1 ? a[0] : a.join(separator);
    return false;
};

module.exports = CreateBuilder;