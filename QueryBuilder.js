/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */


/**
 * Strongly inspired by DBAL QueryBuilder Doctrine 2.0
 *
 * @author Afranio Martins<afranioce@gmail.com>
 * @version 0.1
 *
 */
var _ = require('Avanz/lib/underscore');

/**
 * Creates a new QueryBuilder.
 * @class Represents a QueryBuilder.
 * @returns {QueryBuilder}
 */
function QueryBuilder() {
  "use strict";

  this.VERSION = '0.1';

  /* The query types. */
  this.SELECT = 0;
  this.DELETE = 1;
  this.UPDATE = 2;
  this.INSERT = 3;
  this.CREATE = 4;

  /** The builder states. */
  this.STATE_DIRTY = 0;
  this.STATE_CLEAN = 1;

  this.EQ = '=';
  this.NEQ = '<>';
  this.LT = '<';
  this.LTE = '<=';
  this.GT = '>';
  this.GTE = '>=';
  this.IN = 'IN';
  this.NOT_IN = 'NOT IN';
  this.LIKE = 'LIKE';
  this.NOT_LIKE = 'NOT LIKE';

  /**
   * Constant that represents an AND composite expression
   * @type {string}
   */
  this.TYPE_AND = 'AND';

  /**
   * Constant that represents an OR composite expression.
   * @type {string}
   */
  this.TYPE_OR = 'OR';

  /**
   * The complete SQL string for this query.
   * @type {string}
   */
  var _sql;
  var _type = this.SELECT;
  var _state = this.STATE_CLEAN;
  var _firstResult = null;
  var _maxResults = null;
  var _condition = false;
  var _result;

  /**
   * The object of Connection collected
   * @type {object} object.
   */
  var _db;

  /**
   * The array of SQL parts collected.
   * @type {array}
   */
  var _sqlParts = {
    'select': [],
    'from': [],
    'join': {},
    'set': [],
    'where': [],
    'groupBy': [],
    'having': [],
    'orderBy': [],
    'field': {
      'fields': [],
      'values': []
    }
  };

  var _operators = [
    this.EQ,
    this.NEQ,
    this.LT,
    this.LTE,
    this.GT,
    this.GTE,
    this.IN,
    this.NOT_IN,
    this.LIKE,
    this.NOT_LIKE
  ];

  /**
   * return connection database
   * @return Config connection
   */
  this.getConnection = function() {
    var Config = require('Avanz/Config');
    var conf = new Config();

    return conf.connection;
  };

  /**
   * @return integer
   */
  this.getState = function() {
    return _state;
  };

  /**
   * Execute connection on batabase and execute Query
   *
   * @example
   * <pre>
   * var db = new QueryBuilder()
   * var result = db.select()
   *     .from('table', 'alias')
   *     .where('foo', 1)
   *     .execute()
   *     .fetchAll();
   * </pre>
   * @param {string} sql Query
   * @param {boolean} closeDatabase Case is false return data of database
   * @return {object}
   */
  this.execute = function(sql, closeDatabase) {

    //get SQL to string and execute
    if (_.isUndefined(sql) || _.isEmpty(sql))
      sql = this.getSQL();

    if (_.isUndefined(closeDatabase) || closeDatabase !== false)
      closeDatabase = true;

    //source database
    var database = this.getConnection();

//        Install database
//        _db = Ti.Database.install(database.database, database.name);

    //Open database
    _db = Ti.Database.open(database.name);

    console.log('[QueryBuild] execute, select consulta ao bd', sql);

    _result = _db.execute(sql);

    if (closeDatabase === true) {
      //No iphone após o execute se for executado o fetchAll e se a conexão com
      //o banco estiver fechada ele vai dar erro e se não fechar a conexão com o
      //andoid dá erro
      if (Ti.Platform.osname === 'android') {
        _db.close();
      }
      return this;
    } else {
      return _db;
    }
  };

  /**
   * fetch All results
   *
   * @example
   * <pre>
   * var db = new QueryBuilder()
   * var query = db.select()
   *     .from('user', 'u')
   *     .execute();
   * var rows = query.fetchAll();
   *
   * for(var row in rows){
   *     rows.id
   *     rows.name
   * }
   * </pre>
   * @return {object}
   */
  this.fetchAll = function() {

    if (_.isNull(_result))
      return this;

    var rows = [];
    var index = 0;

    var fieldCount;
    // fieldCount is a property on Android.
    if (Ti.Platform.name === 'android') {
      fieldCount = _result.fieldCount;
    } else {
      fieldCount = _result.fieldCount();
    }

    while (_result.isValidRow()) {
      var row = {};

      for (var i = 0; i < fieldCount; i++) {
        row[_result.getFieldName(i)] = _result.field(i);
      }

      rows.push(row);
      index++;
      _result.next();
    }

    _result.close();

    return rows;
  };
  /**
   * @example
   * <pre>
   * var db = new QueryBuilder()
   * var query = db.select('id', 'name', 'email')
   *     .from('user', 'u')
   *     .execute();
   * var result = query.fetch();
   *
   * alert('has user name: ' + result.name);
   * </pre>
   * @returns {array}
   */
  this.fetch = function() {
    var rows = this.fetchAll();
    if (_.isEmpty(rows))
      return false;

    return _.first(rows);
  };

  /**
   * get SQL string for this query
   *
   * @example
   * <pre>
   * var db = new QueryBuilder()
   * var query =  db.select()
   *     .from('table', 'alias');
   * var queryString = query.getSQL();
   * </pre>
   * @return {String}
   */
  this.getSQL = function() {
    if (_sql !== null && _state === this.STATE_CLEAN) {
      return _sql;
    }

    _sql = '';

    switch (_type) {
      case this.DELETE:
        _sql = getSQLForDelete();
        break;

      case this.UPDATE:
        _sql = getSQLForUpdate();
        break;

      case this.INSERT:
        _sql = getSQLForInsert();
        break;

      case this.SELECT:
      default:
        _sql = getSQLForSelect();
        break;
    }

    _state = this.STATE_CLEAN;

    this.resetQueryParts();

    console.log('getSQL consulta ao bd', _sql);

    return _sql;
  };

  this.setFirstResult = function(firstResult) {
    _state = this.STATE_DIRTY;
    _firstResult = firstResult;
    return this;
  };

  this.getFirstResult = function() {
    return _firstResult;
  };

  this.setMaxResults = function(maxResults) {
    _state = this.STATE_DIRTY;
    _maxResults = maxResults;
    return this;
  };

  this.getMaxResults = function() {
    return _maxResults;
  };

  this.add = function(sqlPartName, sqlPart, append) {
    append || (append = false);

    var isObject = toString.call(sqlPart) == "[object Object]";
    var isMultiple = toString.call(_sqlParts[sqlPartName]) == "[object Object]";

    if (isMultiple && !isObject) {
      sqlPart = [sqlPart];
    }

    _state = this.STATE_DIRTY;
    if (append) {
      if (/field/i.exec(sqlPartName)) {
        _sqlParts[sqlPartName] = {
          'fields': [],
          'values': []
        };
        _.each(_.keys(sqlPart), function(key) {
          _sqlParts[sqlPartName][key] = _.union(_sqlParts[sqlPartName][key], sqlPart[key]);
        });
      } else if (/orderBy|groupBy|select|set/i.exec(sqlPartName)) {
        _sqlParts[sqlPartName] = _.union(_.toArray(_sqlParts[sqlPartName]), _.toArray(sqlPart));
      } else if (isObject && toString.call(sqlPart[_.keys(sqlPart)]) === "[object Object]") {
        var key = _.keys(sqlPart);
        _sqlParts[sqlPartName][key] = _.union(_.toArray(_sqlParts[sqlPartName][key]), [sqlPart[key]]);
      } else if (isMultiple) {
        _.extend(_sqlParts[sqlPartName], [sqlPart]);
      } else {
        _sqlParts[sqlPartName] = _.union(_.toArray(_sqlParts[sqlPartName]), [sqlPart]);
      }

      return this;
    }

    _sqlParts[sqlPartName] = sqlPart;

    return this;
  };
  /**
   * Specifies an item that is to be returned in the query result.
   * Replaces any previously specified selections, if any.
   *
   * @example
   * <pre>
   * var db = new QueryBuilder();
   * var query = db.QueryBuilder()
   *     .select('u.id', 'p.id')
   *     .from('users', 'u')
   *     .leftJoin('u', 'phonenumbers', 'p', 'u.id = p.user_id');
   * </pre>
   * @param {mixed} select The selection expressions.
   * @return QueryBuilder This QueryBuilder instance.
   */
  this.select = function(select) {
    _type = this.SELECT;

    if (_.isUndefined(select)) {
      select = ['*'];
    }

    var selects = _.isArray(select) ? select : arguments;

    return this.add('select', _.toArray(selects), false);
  };

  /**
   * Adds an item that is to be returned in the query result.
   *
   * @example
   * <pre>
   * var db = new QueryBuilder();
   * var query = db.QueryBuilder()
   *     .select('u.id')
   *     .addSelect('p.id')
   *     .from('users', 'u')
   *     .leftJoin('u', 'phonenumbers', 'p', 'u.id = p.user_id');
   * </pre>
   * @param {mixed} select The selection expressions.
   * @return QueryBuilder This QueryBuilder instance.
   */
  this.addSelect = function(select) {
    _type = this.SELECT;

    if (select.length === 0) {
      return this;
    }

    var selects = _.isArray(select) ? select : arguments;

    return this.add('select', _.toArray(selects), true);
  };

  this.count = function() {
    return this.select('COUNT(*)');
  };

  /**
   * Turns the query being built into a bulk delete query that ranges over
   * a certain table.
   *
   * @example
   * <pre>
   * var db = new QueryBuilder();
   * var query = db.delete('users', 'u')
   *     .where('u.id' 1, db.EQ);
   * </pre>
   * @param {string} del The table whose rows are subject to the deletion.
   * @param {string} alias The table alias used in the constructed query.
   * @return QueryBuilder This QueryBuilder instance.
   */
  this.delete = function(del, alias) {
    _type = this.DELETE;

    if (!del) {
      return this;
    }

    return this.add('from', {
      'table': del,
      'alias': alias
    });
  };

  /**
   * Turns the query being built into a bulk update query that ranges over
   * a certain table
   *
   * @example
   * <pre>
   * var db = new QueryBuilder();
   * var query = db.update('users', 'u')
   *     .set('u.password', md5('password'))
   *     .where('u.id', 1);
   * </pre>
   * @param {string} update The table whose rows are subject to the update.
   * @param {string} alias The table alias used in the constructed query.
   * @return {QueryBuilder} This QueryBuilder instance.
   */
  this.update = function(update, alias) {
    _type = this.UPDATE;

    if (!update) {
      return this;
    }

    return this.add('from', {
      'table': update,
      'alias': alias
    });
  };

  this.insert = function(insert) {
    _type = this.INSERT;

    if (!insert) {
      return this;
    }

    return this.add('from', {
      'table': insert
    });
  };

  this.create = function(table) {
    _type = this.CREATE;

    if (!_.isUndefined(table) && !_.isString(table)) {
      return this;
    }

    var CreateBuilder = require('Avanz/misc/CreateBuilder');

    var Create = new CreateBuilder();
    return Create.create(table);
  };

  /**
   * Create and add a query root corresponding to the table identified by the
   * given alias, forming a cartesian product with any existing query roots.
   *
   * @example
   * <pre>
   * var db = new QueryBuilder();
   * var query = db.select('u.id').from('users', 'u');
   * </pre>
   *
   * @param {string} from   The table
   * @param {string} alias  The alias of the table
   * @return {QueryBuilder} This QueryBuilder instance.
   */
  this.from = function(from, alias) {
    return this.add('from', {
      'table': from,
      'alias': alias || (alias = '')
    }, true);
  };

  /**
   * Creates and adds a join to the query.
   *
   * @example
   * <pre>
   * var db = new QueryBuilder();
   * var query = db.select('u.name')
   *     .from('users', 'u')
   *     .join('u', 'phonenumbers', 'p', 'p.is_primary = 1');
   * </pre>
   * @param {string} fromAlias The alias that points to a from clause
   * @param {string} join The table name to join
   * @param {string} alias The alias of the join table
   * @param {string} condition The condition for the join
   * @return {QueryBuilder} This QueryBuilder instance.
   */
  this.join = function(fromAlias, join, alias, condition) {
    condition || (condition = null);

    return this.innerJoin(fromAlias, join, alias, condition);
  };

  /**
   * Creates and adds a join to the query.
   *
   * @example
   * <pre>
   * var db = new QueryBuilder();
   * var query = db.select('u.name')
   *     .from('users', 'u')
   *     .innerJoin('u', 'phonenumbers', 'p', 'p.is_primary = 1');
   * </pre>
   *
   * @param {string} fromAlias The alias that points to a from clause
   * @param {string} join The table name to join
   * @param {string} alias The alias of the join table
   * @param {string} condition The condition for the join
   * @return {QueryBuilder} This QueryBuilder instance.
   */
  this.innerJoin = function(fromAlias, join, alias, condition) {
    condition || (condition = null);

    var obj = {};
    obj[fromAlias] = {
      'joinType': 'inner',
      'joinTable': join,
      'joinAlias': alias,
      'joinCondition': condition
    };

    return this.add('join', obj, true);
  };

  /**
   * Creates and adds a join to the query.
   *
   * @example
   * <pre>
   * var db = new QueryBuilder();
   * var query = db.select('u.name')
   *     .from('users', 'u')
   *     .leftJoin('u', 'phonenumbers', 'p', 'p.is_primary = 1');
   * </pre>
   *
   * @param {string} fromAlias The alias that points to a from clause
   * @param {string} join The table name to join
   * @param {string} alias The alias of the join table
   * @param {string} condition The condition for the join
   * @return {QueryBuilder} This QueryBuilder instance.
   */
  this.leftJoin = function(fromAlias, join, alias, condition) {
    condition || (condition = null);

    var obj = {};
    obj[fromAlias] = {
      'joinType': 'left',
      'joinTable': join,
      'joinAlias': alias,
      'joinCondition': condition
    };

    return this.add('join', obj, true);
  };

  /**
   * Creates and adds a join to the query.
   *
   * @example
   * <pre>
   * var db = new QueryBuilder();
   * var query = db.select('u.name')
   *     .from('users', 'u')
   *     .rightJoin('u', 'phonenumbers', 'p', 'p.is_primary = 1');
   * </pre>
   * @param {string} fromAlias The alias that points to a from clause
   * @param {string} join The table name to join
   * @param {string} alias The alias of the join table
   * @param {string} condition The condition for the join
   * @return {QueryBuilder} This QueryBuilder instance.
   */
  this.rightJoin = function(fromAlias, join, alias, condition) {
    condition || (condition = null);

    var obj = {};
    obj[fromAlias] = {
      'joinType': 'right',
      'joinTable': join,
      'joinAlias': alias,
      'joinCondition': condition
    };

    return this.add('join', obj, true);
  };

  /**
   * Sets a new value for a column in a bulk update query.
   *
   * @example
   * <pre>
   * var db = new QueryBuilder();
   * var query = db.update('users', 'u')
   *     .set('u.password', md5('password'))
   *     .where('u.id = ?');
   * </pre>
   * @param {string} key The column to set.
   * @param {string} value The value, expression, placeholder, etc.
   * @return {QueryBuilder} This QueryBuilder instance.
   */
  this.set = function(key, value) {
    return this.add('set', key + ' = ' + value, true);
  };

  this.where = function(field, value, operator) {
    return this.condition('where', null, field, value, operator);
  };

  this.andWhere = function(field, value, operator) {
    return this.condition('where', this.TYPE_AND, field, value, operator);
  };

  this.orWhere = function(field, value, operator) {
    return this.condition('where', this.TYPE_OR, field, value, operator);
  };

  this.isNull = function(field, type) {
    type || (type = this.TYPE_OR);
    return this.condition('where', type, field, null, 'IS NULL');
  };

  this.isNotNull = function(field, type) {
    type || (type = this.TYPE_OR);
    return this.condition('where', type, field, null, 'IS NOT NULL');
  };

  this.find = function(id) {
    return this.condition('where', null, 'id', id, this.EQ);
  };

  this.findBy = function(field, value) {
    return this.condition('where', null, field, value, this.EQ);
  };

  this.condition = function(condition, type, field, value, operator) {
    value = !_.isNull(value) ? value : '';

    type || (type = _.isNull(type) && _condition === true ? 'AND' : '');

    _condition = true;

    if (_.isArray(value)) {
      operator || (operator = this.IN);
      operator = operator.toUpperCase().trim();
      var con = [this.IN, this.NOT_IN];
      operator = con[_.indexOf(con, operator)];

      value = "('" + value.join("', '") + "')";
    } else {
      value = "'" + value + "'";
    }

    operator = !_.isUndefined(operator) && _.indexOf(_operators, operator) ? operator : this.EQ;

    var parts = field + ' ' + operator + ' ' + value;

    return this.add(condition, {
      'type': type,
      'parts': parts.trim()
    }, true);
  };

  this.groupBy = function(group) {
    if (group.length === 0) {
      return this;
    }

    var groups = _.isArray(group) ? group : arguments;

    return this.add('groupBy', groups, false);
  };

  this.addGroupBy = function(group) {
    if (group.length === 0) {
      return this;
    }

    var groups = _.isArray(group) ? group : arguments;

    return this.add('groupBy', groups, true);
  };

  this.having = function(field, value, operator) {
    return this.condition('having', '', field, value, operator);
  };

  this.andHaving = function(field, value, operator) {
    return this.condition('having', this.TYPE_AND, field, value, operator);
  };

  this.orHaving = function(field, value, operator) {
    return this.condition('having', this.TYPE_OR, field, value, operator);
  };

  this.orderBy = function(sort, order) {
    var append = !_.isEmpty(_sqlParts['orderBy']) ? true : false;

    return this.add('orderBy', [sort + ' ' + (!order ? 'ASC' : order)], append);
  };

  this.addOrderBy = function(sort, order) {
    return this.add('orderBy', [sort + ' ' + (!order ? 'ASC' : order)], true);
  };

  this.field = function(field, value) {
    var fields = {};
    fields[field] = value;
    return this.fields(fields);
  };

  this.fields = function(fields) {
    if (!_.isObject(fields)) {
      return this;
    }

    return this.add('field', {
      'fields': _.keys(fields),
      'values': _.values(fields)
    }, true);
  };

  this.getQueryPart = function(queryPartName) {
    return _sqlParts[queryPartName];
  };

  this.getQueryParts = function() {
    return _sqlParts;
  };

  this.resetQueryParts = function(queryPartNames) {

    _condition = false;
    _maxResults = null;
    _firstResult = null;

    if (_.isUndefined(queryPartNames)) {
      queryPartNames = _.keys(_sqlParts);
    }

    for (var queryPartName in queryPartNames) {
      this.resetQueryPart(queryPartNames[queryPartName]);
    }

    return this;
  };

  this.resetQueryPart = function(queryPartName) {
    _sqlParts[queryPartName] = _.isArray(_sqlParts[queryPartName])
            ? [] : {};

    _state = this.STATE_DIRTY;

    return this;
  };

  /**
   * Converts this instance into an SELECT string in SQL.
   *
   * @return {string} query
   */
  var getSQLForSelect = function() {
    var query = 'SELECT ' + _sqlParts['select'].join(', ') + ' FROM ';

    var fromClauses = {};

    // Loop through all FROM clauses
    _.each(_sqlParts['from'], function(from) {
      var fromClause = from['table'] + ' ' + from['alias'];

      if (!_.isUndefined(_sqlParts['join'][from['alias']])) {
        _.each(_sqlParts['join'][from['alias']], function(join) {
          fromClause += ' ' + join['joinType'].toUpperCase()
                  + ' JOIN ' + join['joinTable'] + ' ' + join['joinAlias']
                  + ' ON ' + (join['joinCondition']);
        });
      }
      fromClauses[from['alias']] = fromClause;

    });

    // loop through all JOIN clauses for validation purpose
    _.each(_sqlParts['join'], function(fromAlias, joins) {
      if (_.isEmpty(fromClauses[fromAlias])) {
        //throw new Error('Error: ' + fromAlias + ' >>> ' + _.keys(fromClauses));
      }
    });

    query += _(fromClauses).values().join(', ')
            + (!_.isEmpty(_sqlParts['where']) ? ' WHERE' + doCondition('where') : '')
            + (!_.isEmpty(_sqlParts['groupBy']) ? 'GROUP BY ' + _sqlParts['groupBy'].join(', ') : '')
            + (!_.isEmpty(_sqlParts['orderBy']) ? 'ORDER BY ' + _sqlParts['orderBy'].join(', ') : '')
            + (!_.isEmpty(_sqlParts['having']) ? 'HAVING' + doCondition('having') : '');
    return (_.isNull(_maxResults) && _.isNull(_firstResult))
            ? query
            : doOffset(query, _maxResults, _firstResult);
  };

  var doCondition = function(condition) {
    var conditions = '';
    _.each(_sqlParts[condition], function(cond) {
      conditions += cond['type'] + ' (' + cond['parts'] + ') ';
    });
    return conditions;
  };

  var doOffset = function(query, limit, offset) {
    if (!_.isNull(limit)) {
      query += ' LIMIT ' + parseInt(limit, 10);
    }

    if (offset !== null) {
      offset = parseInt(offset, 10);
      if (offset < 0) {
        throw new Error("Error: LIMIT argument offset=offset is not valid");
      }
      query += ' OFFSET ' + offset;
    }

    return query;
  };

  /**
   * Converts this instance into an UPDATE string in SQL.
   *
   * @return {string} query
   */
  var getSQLForUpdate = function() {
    var table = _sqlParts['from']['table']
            + (_sqlParts['from']['alias'] ? ' ' + _sqlParts['from']['alias'] : '');
    var query = 'UPDATE ' + table
            + ' SET ' + _sqlParts['set'].join(', ')
            + (!_.isNull(_sqlParts['where']) ? ' WHERE' + doCondition('where') : '');

    return query;
  };

  this.toString = function() {
    return this.getSQL();
  };

  /**
   * Converts this instance into a DELETE string in SQL.
   *
   * @return {string} query
   */
  var getSQLForDelete = function() {
    var table = _sqlParts['from']['table'] + (_sqlParts['from']['alias'] ? ' '
            + _sqlParts['from']['alias'] : '');
    var query = 'DELETE FROM ' + table
            + (!_.isNull(_sqlParts['where']) ? ' WHERE' + doCondition('where') : '');

    return query;
  };

  /**
   * Converts this instance into a INSERT string in SQL.
   *
   * @return string
   */
  var getSQLForInsert = function() {
    var query = 'INSERT INTO ' + _sqlParts['from']['table'];
    if (!_.isUndefined(_sqlParts['field']['fields']))
      query += '(' + _sqlParts['field']['fields'].join(', ') + ") VALUES ('";

    query += _sqlParts['field']['values'].join("', '") + "')";

    return query;
  };
}

module.exports = QueryBuilder;